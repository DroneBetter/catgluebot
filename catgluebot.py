import discord
import random
import re
import importlib

import getuser
import setmirror
import messageutils

guilds = {}

DEFAULT_NICK = "catglue"


class MyClient(discord.Client):
  async def on_ready(self):
    print('Logged on as', self.user)

  async def on_message(self, message: discord.Message):
    global guilds
    if message.guild not in guilds:
      guilds[message.guild] = {}
    cur = guilds[message.guild]
    # print(message.content)
    if message.author == self.user:
      if random.randint(0, 1):
        return
    if "nicks" not in cur:
      cur["nicks"] = {DEFAULT_NICK}
    for nick in cur["nicks"]:
      if message.content.lower().startswith(nick + ", "):
        com = message.content[len(nick + ", "):]
        await self.commandentrypoint(self, message, cur, com)
        return


async def command_echo(clientstate: MyClient, message: discord.Message, cur: dict, args):
  await message.channel.send(args.group(1))
  pass


async def command_rest(clientstate: MyClient, message: discord.Message, cur: dict, args):
  if message.author.id == 408756971844403224:
    await message.channel.send("I acknowledge your command.")
    await clientstate.close()
  return


async def command_rename(clientstate: MyClient, message: discord.Message, cur: dict, args):
  if message.author.id == 408756971844403224:
    n = args.group(2)
    if args.group(1) == "you":
      await message.channel.send("I may henceforth be summoned by %s." % n)
      cur["nicks"].add(n)
    elif args.group(1) == "remove":
      if n != DEFAULT_NICK and n in cur["nicks"]:
        await message.channel.send("I will no longer be summoned by %s." % n)
        cur["nicks"].remove(n)
  return


async def command_getuser(clientstate: MyClient, message: discord.Message, cur: dict, args):
  importlib.reload(getuser)
  await getuser.getuser_inner(clientstate, message, cur, args)
  return


async def command_makemirror(clientstate: MyClient, message: discord.Message, cur: dict, args):
  importlib.reload(setmirror)
  await setmirror.setmirror_inner(clientstate, message, cur, args)
  return


async def command_deletemessage(clientstate: MyClient, message: discord.Message, cur: dict, args):
  importlib.reload(messageutils)
  await messageutils.deletemessage_inner(clientstate, message, cur, args)
  return


async def command_revert(clientstate: MyClient, message: discord.Message, cur: dict, args):
  importlib.reload(setmirror)
  await setmirror.revert_inner(clientstate, message, cur, args)
  return


async def command_pinmessage(clientstate: MyClient, message: discord.Message, cur: dict, args):
  importlib.reload(messageutils)
  await messageutils.pinmessage_inner(clientstate, message, cur, args)
  return

RULES = [(re.compile(r"^rest$"), command_rest),
         #  (re.compile(r"^say (.*)$"), command_echo),
         (re.compile(r"^(you) shall be known as (.*)$"), command_rename),
         (re.compile(r"^(remove) the name (.*)$"), command_rename),
         #  (re.compile(r"^fetch messages$"), command_fetchmessages),
         (re.compile(r"^find user by id (.*)$"), command_getuser),
         (re.compile(r"^(start) catglue to channel (.*)$"), command_makemirror),
         (re.compile(r"^(stop) catglue$"), command_makemirror),
         (re.compile(r"^revert last update$"), command_revert),
         #  (re.compile(r"^delete by id (\d*) from (\d*)$"), command_deletemessage),
         #  (re.compile(r"^(pin) by id (\d*) from (\d*)$"), command_pinmessage),
         #  (re.compile(r"^(unpin) by id (\d*) from (\d*)$"), command_pinmessage),
         ]


async def commandentrypoint(clientstate: MyClient, message: discord.Message, cur: dict, com: str):
  for e, f in RULES:
    m = re.match(e, com)
    if m is not None:
      await f(clientstate, message, cur, m)
      return
  return


intents = discord.Intents.all()
client = MyClient(intents=intents)
client.commandentrypoint = commandentrypoint
token = open("token.txt").read().strip()
try:
  client.run(token)
except RuntimeError:
  pass

print(":)")
