import discord
import discord.abc


async def send_large_message(channel: discord.abc.Messageable, text: str):
  if "\n" in text:
    tp = text.split("\n")
    cur = []
    for i in tp:
      if len("\n".join(cur + [i])) > 2000:
        await channel.send("\n".join(cur))
        cur = []
      if len(i) > 2000:
        await send_large_message(channel, i)
      else:
        cur += [i]
    await channel.send("\n".join(cur))
    return
  if " " in text:
    tp = text.split(" ")
    cur = []
    for i in tp:
      if len(" ".join(cur + [i])) > 2000:
        await channel.send(" ".join(cur))
        cur = []
      if len(i) > 2000:
        await send_large_message(channel, i)
      else:
        cur += [i]
    await channel.send(" ".join(cur))
    return
  cur = []
  for i in text:
    if len("".join(cur + [i])) > 2000:
      await channel.send("".join(cur))
      cur = []
    cur += [i]
  await channel.send("".join(cur))
  return
