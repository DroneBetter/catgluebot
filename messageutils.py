from http import client
import re

import discord
import discord.abc


async def deletemessage_inner(clientstate: discord.Client, message: discord.Message, cur: dict, args):
  if message.author.id == 408756971844403224:
    try:
      s = int(args.group(1))
      c = int(args.group(2))
      ch: discord.abc.Messageable = await clientstate.fetch_channel(c)
      u: discord.Message = await ch.fetch_message(s)
      await u.delete()
      await message.channel.send("success")
    except Exception as e:
      await message.channel.send("fail %s" % str(e))


async def pinmessage_inner(clientstate: discord.Client, message: discord.Message, cur: dict, args):
  if message.author.id == 408756971844403224:
    try:
      s = int(args.group(2))
      c = int(args.group(3))
      ch: discord.abc.Messageable = await clientstate.fetch_channel(c)
      u: discord.Message = await ch.fetch_message(s)
      if args.group(1) == "pin":
        await u.pin()
      else:
        await u.unpin()
      await message.channel.send("success")
    except Exception as e:
      await message.channel.send("fail %s" % str(e))
