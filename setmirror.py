import re

import discord
import discord.abc

import asyncio

import os
import time
import hashlib
import aiohttp

notifier_role = "876266912663892030"

total_wait = 3600  # seconds
compare_wait = 60
get_wait = total_wait - compare_wait

fail_time = 30
post_fail_time = 30
ICON_URL = "https://cdn.discordapp.com/avatars/876266072125358090/82e14c7e2b9e9bcc84b08c477007ec60.png"

symmetries = ["D8_4", "D8_1", "D4_+4", "D4_+2", "D4_+1", "D4_x4", "D4_x1", "D2_+2", "D2_+1", "D2_x",
              "C4_4", "C4_1", "C2_4", "C2_2", "C2_1", "H4_+4", "H4_+2", "H4_+1", "G2_4", "G2_2", "G2_1", "C1", "G1"]

names = {**{name.split("\"")[1]: name.split("\"")[3] for name in open("all-common-names.txt", "r").readlines()[1:-1]},
         **{name.split("\"")[1]: name.split("\"")[3] for name in open("wiki-names.txt", "r").readlines()}}  # get common names

colours = {
    "xs": 0xFFFFCE,
    "xp": 0xCECEFF,
    "xq": 0xCEFFCE,
    "yl": 0xFFCECE,
    "zz": 0xFFCEFF,
    "ov": 0xCECECE,
    "PA": 0xFFFFFF,
    "me": 0xCEFFFF
}


async def rget(url):  # get but it retries if fail
  backoff = fail_time
  while True:
    print("getting", url)
    try:
      statusCode = None
      respText = None
      timeout = aiohttp.ClientTimeout(total=60)
      async with aiohttp.ClientSession(timeout=timeout) as session:
        timeout2 = aiohttp.ClientTimeout(total=10)
        async with session.get(url, timeout=timeout2) as resp:
          statusCode = resp.status
          respText = await resp.text()
      if statusCode == 200:
        return respText
      else:
        print(statusCode)
    except Exception as e:
      print(e)
    print("Server Error, sleeping for " + str(backoff) + " seconds...")
    await asyncio.sleep(backoff)
    backoff += fail_time

async def notify(apgcode, occurrences, occurrences_old, symmetry, pcount):  # notify about object
  print(         apgcode, occurrences, occurrences_old, symmetry, pcount)
  samples = await rget(f"https://catagolue.hatsya.com/textsamples/{apgcode}/b3s23")
  soup = samples.split(symmetry + "/")[-1].split("\n")[0]

  # Get additional information
  soups = await rget(f"https://catagolue.hatsya.com/attribute/{apgcode}/b3s23")
  soup_index = soups.rfind(soup)

  prefix = apgcode.split("_")[0]

  if soup_index != -1:
    found, owner = soups.split(" on ")[-1].split(" and is owned by ")
    owner = owner.split("\n")[0]
    found.replace(" at ", "")
    if owner.find("@") > 2:
      owner = owner.split("@")[0]
  else:  # FALLBACK: Get haul manually and find discoverer.
    for r in [14, 48, 12]:
      root = soup[:r]
      haul = await rget("https://catagolue.hatsya.com/haul/b3s23/" + symmetry +
                        "/" + hashlib.md5(root.encode("ascii")).hexdigest())
      if haul.find("submitted") != -1:
        found = haul.split("UTC")[0].split(" on ")[-1] + "UTC"
        owner = haul.split("submitted by ")[1].split(">")[1].split("<")[0]
        break
      else:
        found = "???"
        owner = "???"

  name = names.get(apgcode)
  apgcodetext = "["+(name.split("!")[0] if name else apgcode)+"](https://catagolue.hatsya.com/object/"+apgcode+"/b3s23)"
  field = (lambda n,v: {"name": n,"value": v,"inline": True})
  embed={"embeds": [{
           "title": "Object found!",
           "description": (lambda p: "A"+"n"*(p[0] in "aeiou")+" "+p)(apgcode.split("_")[0] if occurrences_old==0 else prefix)+" with "+("zero previous" if occurrences_old==0 else "a low number of")+" occurrences has been located"+('!' if occurrences_old==0 else '.'),
           "color": colours[apgcode[:2]],
           "footer": {
             "icon_url": "https://cdn.discordapp.com/avatars/876266072125358090/82e14c7e2b9e9bcc84b08c477007ec60.png",
             "text": "catgIue.py"
           },
           "fields": list(starmap(field,(("apgcode",apgcodetext),
                                         ("Occurrences",str(occurrences)+" ("+str(occurrences_old)+")"),
                                         ("Symmetry",symmetry),
                                         ("Soup","["+soup+"](https://catagolue.hatsya.com/hashsoup/"+symmetry+"/"+soup+"/b3s23)" if found != "???" else "???"),
                                         ("Discoverer",owner),
                                         ("Time",found))))
      }]
  }

  if not isinstance(pcount, bool) and apgcode[0] in "xy":
    # List USO
    embed["embeds"][0]["fields"].append({"name": "Unique Similar Objects", "value": pcount, "inline": True})
    # ping for new object
  embed["content"] = ("<@&"+notifier_role+"> ")*(occurrences_old == 0)+prefix+" get!"
  response = rpost(webhook_url, embed)

# checks whether a given symmetry has been updated in the last wait_time
async def is_active(symmetry):
  gg = await rget("https://catagolue.hatsya.com/texthaul/b3s23/" +
                  symmetry)
  data = gg.split(" ")[2]
  last_haul = time.strptime(data, "%Y-%m-%dT%H:%M:%S")
  last_call = time.gmtime(time.time() - total_wait)
  return last_haul > last_call


def get_pop(apgcode):  # gets population of apgcode. this code isn't used but i wrote anyway
  if apgcode[:2] == "xs":
    return int(apgcode[2:apgcode.find("_")])
  elif apgcode[:2] in ["me", "ov", "PA", "yl", "zz"]:
    return 0
  letters = "0123456789abcdefghijklmnopqrstuv"
  population, space = 0, 0
  code = apgcode.split("_")[1]
  for c in code:
    if space:
      space = False
    elif c == "y":
      space = True
    elif c not in "wxz":
      population += letters.find(c).bit_count()
  return population



async def diff(symmetry):  # compares textcensus files
  (old_file,new_file) = map(lambda s: open(symmetry+s, "r").read(),("_old","_new"))
  (old,new) = map(lambda f: {i.split(",")[0][1:-1]: int(i.split(",")[1][1:-1]) for i in f.splitlines()[1:] if i[0] == "\""},(old_file,new_file))

  amounts = {}  # Only check one object per prefix

  if symmetry not in ["C1", "G1"]:
    largests = [0,0,0,0] #still_life, messless, methuselah, megasized

    # Find largest still life in symmetry
    for apgcode in old.keys():
      for i,prefix in enumerate("xs","messless","methuselah","megasized"):
        if apgcode[:len(prefix)] == prefix:
          pop = int(apgcode[len(prefix):len(apgcode)-bool(i)].split("_")[bool(i)])
          if pop>largests[i]:
            largests[i] = pop

    # This has been nested inside diff so it can access the full file text.
  def is_notable(apgcode, old_num, symmetry):
    name = names.get(apgcode)
    if old_num >= 10 or (symmetry not in ["C1", "G1"] and (old_num >= 1 or apgcode[:4] == "xp2_") and not name):
      return False

    label = apgcode[:2]
    prefix = apgcode.split("_")[0]
    if apgcode != prefix:
      prefix += "_"

    if symmetry in ["C1", "G1"]:
      # if symmetry == "G1":
      #	samples = rget("https://catagolue.hatsya.com/textsamples/" + apgcode + "/b3s23").text
      #	if samples[:3] == "C1/" or samples.find("\nC1/") != -1:
      #		return False
      return(bool( name
                  or
                    label == "xs"
                   and
                    (lambda pop: pop >= 33 and old_num<10 or pop < =  16)(int(prefix[2:-1]))
                  or
                    label == "xp"
                   and
                    (lambda per: per>2 and old_num<10 or get_pop(apgcode)<18 and old_num<10)(int(prefix[2:-1]))
                  or
                   old_num<10))

    if amounts.get(prefix, None):
      pcount = amounts[prefix]
    else:
      pcount = old_file.count(prefix)
      amounts[prefix] = pcount

    return ( pcount
            if name and not( prefix[:2] == "xs"      and int(prefix[2:-1])  <largests[0]
                            or
                             prefix == "messless_"   and int(apgcode[9:-1]) <largests[1]
                            or
                             prefix == "methuselah_" and int(apgcode[11:-1])<largests[2]
                            or
                             prefix == "megasized_"  and int(apgcode[10:-1])<largests[3]) else
             False)

  diff = {}
  ret = []
  for k in new:
    # print(k)
    old_num = old.get(k, 0)
    diff[k] = new[k] - old_num
    pcount = is_notable(k, old_num, symmetry)
    if diff[k] == 0 or pcount is False:  # pcount only returned if notable
      diff.pop(k, None)
    else:
      print(k)
      ret += [await notify(k, new[k], old_num, symmetry, pcount)]

async def get_new(symmetry):  # gets new textcensus
  if os.path.exists(f"census/{symmetry}_old"):
    os.remove(f"census/{symmetry}_old")
  if os.path.exists(f"census/{symmetry}_new"):
    os.rename(f"census/{symmetry}_new", f"census/{symmetry}_old")
  gg = await rget("https://catagolue.hatsya.com/textcensus/b3s23/" + symmetry + "/summary"*(symmetry not in ["C1", "G1"]))
  open(f"census/{symmetry}_new", "w+").write(gg)


def revert(symmetry):
  if os.path.exists(f"census/{symmetry}_old"):
    if os.path.exists(f"census/{symmetry}_new"):
      os.remove(f"census/{symmetry}_new")
    os.rename(f"census/{symmetry}_old", f"census/{symmetry}_new")


SLEEPING = True


async def catglueRun(channel: discord.abc.Messageable):
  try:
    first_time = True
    while True:
      tx = get_wait - (time.time() - (compare_wait)) % total_wait
      if tx > 10:
        SLEEPING = True
        await asyncio.sleep(tx - 10)
        SLEEPING = False
        await asyncio.sleep(10)
      else:
        SLEEPING = False
        await asyncio.sleep(tx)
      actives = []
      for symmetry in symmetries:
        if first_time or await is_active(symmetry):
          actives.append(symmetry)
          await get_new(symmetry)
          await asyncio.sleep(0.5)
          print("Got " + symmetry)
      print("Done " + str(time.time()))
      # time.sleep(compare_wait)
      ress = []
      for symmetry in actives:
        if os.path.exists(f"census/{symmetry}_old"):
          ress += await diff(symmetry)
        print("Compared " + symmetry)
      print("Done " + str(time.time()))
      print(len(ress))
      for content, embed in ress:
        await channel.send(content=content, embed=embed)
        await asyncio.sleep(1)
      first_time = False
  except Exception as e:
    print("fail %s" % str(e))
  return


async def setmirror_inner(clientstate: discord.Client, message: discord.Message, cur: dict, args):
  if message.author.id == 408756971844403224:
    try:
      if args.group(1) == "start":
        s = int(args.group(2))
        u: discord.abc.Messageable = await clientstate.fetch_channel(s)
        cur["mirrors"] = u
        cur["catgluethread"] = asyncio.create_task(catglueRun(u))
        await message.channel.send("started catglue, message channel: %s" % u.name)
      elif args.group(1) == "stop":
        if "mirrors" in cur:
          del cur["mirrors"]
          await message.channel.send("stopping catglue...")
          while not SLEEPING:
            asyncio.sleep(10)
          cur["catgluethread"].cancel()
          del cur["catgluethread"]
          await message.channel.send("stopped catglue")
        else:
          await message.channel.send("catglue is not running")
    except Exception as e:
      await message.channel.send("fail %s" % str(e))


async def revert_inner(clientstate: discord.Client, message: discord.Message, cur: dict, args):
  if message.author.id == 408756971844403224:
    try:
      for symmetry in symmetries:
        revert(symmetry)
      await message.channel.send("ok")
    except Exception as e:
      await message.channel.send("fail %s" % str(e))