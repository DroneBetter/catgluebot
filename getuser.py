import re

import discord


async def getuser_inner(clientstate: discord.Client, message: discord.Message, cur: dict, args):
  if message.author.id == 408756971844403224:
    try:
      s = int(args.group(1))
      u: discord.User = await clientstate.fetch_user(s)
      await message.channel.send("found <@%s> %s" % (u.id, u.name + '#' + str(u.discriminator)))
    except Exception as e:
      await message.channel.send("fail %s" % str(e))
